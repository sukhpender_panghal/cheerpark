package com.example.cheerpark.ui.home

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cheerpark.R
import com.example.cheerpark.ui.adapter.HomeListAdapter
import com.example.cheerpark.ui.model.MatchList
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.header.*


class HomeActivity : AppCompatActivity() {

    private val db = Firebase.firestore
    private val dbDocs = db.collection("matches")
    var adapter = HomeListAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        recycler_view_home.layoutManager = LinearLayoutManager(this)
        recycler_view_home.adapter = adapter


        dbDocs.get().addOnCompleteListener { document ->
            if (document.isSuccessful) {
                val document = document.result

                if (document != null) {
                    Log.e(
                        "firebase", "matches"
                                + Gson().toJson(document.toObjects(MatchList::class.java))
                    )
                    val list = ArrayList<MatchList>()
                    for (childDocument in document.documents) {
                        if (childDocument.exists()) {
                            val documentReference1 = FirebaseFirestore
                                .getInstance().collection("matches").document(childDocument.id)
                            documentReference1.get()
                                .addOnSuccessListener { documentSnapshot ->

                                    val typ1 = documentSnapshot.toObject(MatchList()::class.java)
                                    typ1?.id = childDocument.id
                                    if (typ1 != null) {
                                        list.add(typ1)
                                        adapter.addItem(typ1)
                                    }
                                }
                        }
                    }
                    adapter.addList(list)
                    adapter.notifyDataSetChanged()
                }
            }
        }
        edt_search_home.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                adapter.filter.filter(p0)

            }

        })
    }
}


