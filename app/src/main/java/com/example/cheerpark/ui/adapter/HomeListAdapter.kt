package com.example.cheerpark.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.cheerpark.R
import com.example.cheerpark.ui.CheerActivity
import com.example.cheerpark.ui.model.MatchList
import kotlinx.android.synthetic.main.custom_view.view.*

class HomeListAdapter(val context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {

    private var data: ArrayList<MatchList> = ArrayList()
    private var datafiltered: ArrayList<MatchList> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val inflatedView = parent.inflate(R.layout.custom_view, false)
        return ViewHolder(inflatedView)
    }

    fun addItem(item: MatchList) {
        data.add(item)
        datafiltered.add(item)
        notifyDataSetChanged()
    }

    fun addList(item: ArrayList<MatchList>) {
        data.clear()
        datafiltered.clear()
        data.addAll(item)
        datafiltered.addAll(item)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return datafiltered.size
    }

    private fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
        return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val matchList = datafiltered[position]
        holder.itemView.txt_team_names.text = matchList.TeamA + " vs " + matchList.TeamB
        Glide.with(context)
            .load(matchList.banner_image)
            .into(holder.itemView.images1)

        holder.itemView.images1.setOnClickListener(View.OnClickListener() {
            val matchList = datafiltered[position]
            val intent = Intent(context, CheerActivity::class.java)
            intent.putExtra("data", matchList)
            context.startActivity(intent)

        })
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(p0: CharSequence?): FilterResults {
                val newData: ArrayList<MatchList> = ArrayList()

                if (p0 == null || p0.isEmpty()) {
                    newData.addAll(data)
                } else {
                    val filterPattern = p0.toString().toLowerCase().trim()
                    for (movie in data) {
                        if (movie.TeamA?.toLowerCase()!!
                                .startsWith(filterPattern) || movie.TeamB?.toLowerCase()!!
                                .startsWith(filterPattern)
                        ) {
                            newData.add(movie)
                        }
                    }

                }
                val results = FilterResults()
                results.values = newData

                return results
            }

            override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
                datafiltered.clear()
                datafiltered = p1?.values as ArrayList<MatchList>
                notifyDataSetChanged()
            }

        }
    }

}