package com.example.cheerpark.ui

import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.cheerpark.R
import com.example.cheerpark.ui.model.MatchList
import com.google.firebase.database.DatabaseReference
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_cheer.*

class CheerActivity : AppCompatActivity() {

    private val db = Firebase.firestore
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cheer)
        var user_token = ""
        val instanceId = FirebaseInstanceId.getInstance().instanceId
        instanceId.addOnCompleteListener {
            if (it.isSuccessful)
                user_token = it.result?.token ?: ""
        }
        val intnt = intent
        val match = intnt.getParcelableExtra<MatchList>("data")
        Log.e("errormatch", "" + match)
        Glide.with(this).load(match?.team_A_logo).into(img_logoA)
        Glide.with(this).load(match?.team_B_logo).into(img_logoB)
        btn_cheersA.text = match?.team_A_suppoters.toString()
        btn_cheersB.text = match?.team_B_suppoters.toString()
        onClick()

        /**pushing value to firebase*/
        btn_cheersA.setOnClickListener {
            if (match?.fcm?.contains(user_token) == false) {
                val mediaPlayer: MediaPlayer? = MediaPlayer.create(this, R.raw.cheer)
                mediaPlayer?.start()
                val cheerCount = btn_cheersA.text.toString()
                var myInt: Int = if(cheerCount.isEmpty())0 else cheerCount.toInt()
                myInt++
                match.team_A_suppoters = myInt
                (match.fcm as ArrayList).add(user_token)
                db.collection("matches")
                    .document(match!!.id!!)
                    .set(match!!)
                btn_cheersA.text = myInt.toString()

            } else {
                Toast.makeText(this, "Already Cheered", Toast.LENGTH_SHORT).show()
            }
        }

        btn_cheersB.setOnClickListener {
            if (match?.fcm?.contains(user_token) == false) {
                val mediaPlayer: MediaPlayer? = MediaPlayer.create(this, R.raw.cheer)
                mediaPlayer?.start()
                val cheerCount = btn_cheersB.text.toString()
                var myInt: Int = if(cheerCount.isEmpty())0 else cheerCount.toInt()
                myInt++
                match.team_B_suppoters = myInt
                (match.fcm as ArrayList).add(user_token)
                db.collection("matches")
                    .document(match!!.id!!)
                    .set(match!!)
                btn_cheersB.text = myInt.toString()

            } else {
                Toast.makeText(this, "Already Cheers", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun onClick() {
        btn_bck.setOnClickListener {
            onBackPressed()
        }
    }

}